source "https://rubygems.org"
ruby "2.3.4"

group :production do
  gem "htmlcompressor"
end

group :test do
  # gem "capybara"
  gem "rspec-rails"
  gem "factory_girl_rails"
  gem "rspec-html-matchers"
  gem "simplecov", :require => false
  gem "database_cleaner"
  gem "webmock"
end

group :development do
  gem "web-console", "~> 2.0"
  gem "meta_request"
  gem "spring"
  gem "rails-erd"
  gem "annotate"
  gem "mailcatcher"
  gem "xray-rails"
end

group :development, :test do
  gem "byebug"
  gem "colorize", "~> 0.8.1"
  gem "pronto", git: "https://github.com/vovka/pronto.git", branch: "master"
  gem "pronto-rubocop", require: false
  gem "pronto-flay", require: false
  gem "pronto-rails_best_practices", require: false
  gem "pronto-rails_schema", require: false
  # gem "pronto-scss", require: false
  gem "bullet"
  gem "active_record_query_trace"
  gem "capistrano", "~> 3.6"
  # gem "capistrano-rbenv", "~> 2.0"
  gem "capistrano-rvm"
  gem "capistrano-rails", "~> 1.2"
  gem "capistrano3-unicorn"
  gem "capistrano-figaro-yml"
  gem "capistrano-sidekiq"
end

gem "inline_svg"
gem "omniauth"
gem "omniauth-facebook"
gem "omniauth-google-oauth2"
gem "omniauth-linkedin"
gem "omniauth-twitter"
gem "rails", "4.2.6"
gem "unicorn"
gem "pg", "~> 0.18.4"
gem "sass-rails", "~> 5.0"
gem "uglifier", ">= 1.3.0"
gem "coffee-rails", "~> 4.1.0"
gem "activeadmin", git: "https://github.com/activeadmin/activeadmin"
gem "devise",           "~> 4.2"
gem "devise_invitable", "~> 1.7.0"
gem "jquery-rails"
gem "turbolinks"
gem "jbuilder", "~> 2.0"
gem "sdoc", "~> 0.4.0", group: :doc
gem "font-awesome-rails"
gem "carrierwave"
gem "cloudinary"
gem "mini_magick"
gem "simple_form", "~> 3.2", ">= 3.2.1"
gem "figaro", "~> 1.1", ">= 1.1.1"
gem "bower-rails", "~> 0.11.0"
gem "pundit"
gem "countries", require: "countries/global"
gem "country_select"
gem "draper"
gem "angular_rails_csrf"
gem "rails-i18n"
gem "faker"
gem "whenever"
gem "activemerchant"
gem "sidekiq"
gem "icalendar"
gem "acts_as_list"
gem "activeadmin_reorderable"
gem "geocoder"
gem "validates_zipcode"
gem "wicked_pdf"
gem "wkhtmltopdf-binary"
gem "phonelib"
gem "data_migrate"
gem "backup"
gem "i18n-active_record", :require => "i18n/active_record"
gem "blockchain-lite"
