$(document).ready(function() {
  $('.btn_menu').click(function() {
    $('.menu ul').slideToggle();
  });
  $("#owl-example").owlCarousel({
    autoPlay: false,
    itemsDesktopSmall: [900, 1],
    itemsTablet: [600, 1], //2 items between 600 and 0;
  });
  $("#owl-example_mobile_items").owlCarousel({
    navigationText: false,
    items: 14.5, //10 items above 1000px browser width
    itemsDesktop: [1515, 12], //5 items between 1000px and 901px
    itemsDesktopSmall: [1240, 8], // betweem 900px and 601px
    itemsTablet: [800, 4.7],
    itemsTabletSmall: [550, 3.8],
    itemsMobile: [390, 2.8], //2 items between 600 and 0;
  });

  $('.btn_menu').click(function() {
    $(this).toggleClass('active');
    $('.menu_mobile').toggleClass('active');
  });
});
function reject_popup() {
  var x = document.getElementById("reject_popup");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
