class ItemsController < ApplicationController
  layout 'item', except: [:show_for_company]

  before_action :set_item, only: [:show, :edit, :update, :destroy, :transfer,
                                  :receive, :show_pdf]
  before_action :authenticate_user_or_company!
  before_action :set_order, only: %i( show dashboard )

  def index
    @items = Item.unscoped.where(user: current_user_or_company)
  end

  def dashboard
    @items = Item.unscoped.where(user: current_user_or_company)
    @services = Service.unscoped
      .includes(:item, :company, :approver, :action_kinds, :service_fields => :service_kind)
      .where(item: @items)
      .joins("LEFT JOIN service_fields ON services.id = service_fields.service_id")
      .joins("LEFT JOIN service_kinds ON service_fields.service_kind_id = service_kinds.id")
      .joins("LEFT JOIN service_action_kinds ON services.id = service_action_kinds.service_id")
      .joins("LEFT JOIN action_kinds ON service_action_kinds.action_kind_id = action_kinds.id")
      .order(dashboard_order_params)
      .decorate
  end

  def show
    @items = Item.unscoped.where(user: current_user_or_company)
    @services = @item.services.includes(:service_fields, :action_kinds)
      .joins("LEFT JOIN service_fields ON services.id = service_fields.service_id")
      .joins("LEFT JOIN service_kinds ON service_fields.service_kind_id = service_kinds.id")
      .joins("LEFT JOIN service_action_kinds ON services.id = service_action_kinds.service_id")
      .joins("LEFT JOIN action_kinds ON service_action_kinds.action_kind_id = action_kinds.id")
      .order(dashboard_order_params)
      .decorate
    respond_to do |format|
      format.html { render "dashboard" }
      format.csv { send_data Item.to_csv(@item) }
    end
  end

  def new
    @item = current_user_or_company.items.build
    @item.check_user_attributes
    @items = [@item]
    @attributes = AttributeKind.all
  end

  def create
    @items = current_user_or_company.items
    @item = Item.new(item_params)
    @item.user = current_user_or_company

    @item.characteristics = characteristics_params.map do |key, value|
      attribute_kind = @item.category.attribute_kinds.find(key)
      Characteristic.new(attribute_kind: attribute_kind, value: value)
    end if characteristics_params.any?

    if @item.save
      blockchain_transaction!
      redirect_to @item, notice: t(".notice")
    else
      render :new
    end
  end

  def edit
    @items = [@item]
    authorize @item
    @services = current_user_or_company.services.includes(:service_fields, :action_kinds).decorate
  end

  def update
    authorize @item

    if characteristics_params.any?
      characteristics_params.select { |_, v| v.present? }.map do |key, value|
        characteristic = @item.characteristics.find { |c| c.attribute_kind_id == key.to_i }
        if characteristic.present?
          if characteristic.value.blank?
            characteristic.update(value: value)
          end
        else
          attribute_kind = @item.category.attribute_kinds.find(key)
          @item.characteristics << Characteristic.new(attribute_kind: attribute_kind, value: value)
        end
      end
    end

    if @item.update(update_item_params)
      redirect_to @item
    else
      render :edit
    end
  end

  def destroy
    authorize @item
    if current_user_or_company.valid_password?(params[:item][:password])
      @item.destroy
      redirect_to dashboard_path, notice: t(".success")
    else
      redirect_to :back, notice: t(".password_invalid")
    end
  end

  def dashboard_company
    @services = current_company.assigned_services.includes(:item)
    @items = @services.map(&:item).uniq
    render "empty_items_services" if @items.blank?
  end

  def show_for_company
    @item = Item.find_by(token: params[:token]) || Item.new
    authorize @item
  end

  def get_attributes
    @item = if params[:item_id].present?
      Item.unscoped.where(id: params[:item_id]).first.tap { |item| authorize item }
    else
      current_user_or_company.items.build
    end
    @item = @item.decorate context: { category_id: params[:category_id],
                                      brand_option_id: params[:brand_option_id] }
  end

  def transfer
    authorize @item
    validator = ItemTransferValidator.new(params)
    if validator.valid?
      TransferService.new(@item, params[:user_identifier], current_user_or_company).perform
      flash[:notice] = t(".success")
    else
      flash[:error] = validator.errors.join(". ")
    end
    redirect_to edit_item_path(@item)
  end

  def receive
    authorize @item
    @item.update user: current_user_or_company, transferring_to: nil
    redirect_to action: :index
  end

  def show_pdf
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "Distodi_report",
        header: { html: { template: "items/pdf_header.html.erb" } },
        template: "items/show_pdf.html.erb",
        layout: 'pdf.html',
        footer: { html: { template: "items/pdf_footer.html.erb" } }
      end
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_item
    @item = Item.unscoped.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def item_params
    params.require(:item).permit(:title, :picture, :picture2, :picture3, :picture4, :picture5, :category_id, :token, :comment)
  end

  def update_item_params
    params.require(:item).permit(:title, :picture, :picture2, :picture3, :picture4, :picture5, :comment)
  end

  def characteristics_params
    category = if @item.present?
      @item.category
    elsif item_params[:category_id].present?
      Category.find(item_params[:category_id])
    end
    attribute_kind_ids = if category.present?
      category.attribute_kinds.pluck(:id).map(&:to_s)
    else
      []
    end
    params.require(:item).require(:characteristics).permit(attribute_kind_ids)
  end

  def set_order
    @order = Struct.new(:service_kind, :service_kind?, :service_type, :service_type?)

    service_kind_directions = Struct.new(:direction, :opposite_direction).new(
      *(order_params.column != "service_kind" || order_params.direction == "desc" ? ["desc", "asc"] : ["asc", "desc"])
    )
    service_type_directions = Struct.new(:direction, :opposite_direction).new(
      *(order_params.column != "service_type" || order_params.direction == "desc" ? ["desc", "asc"] : ["asc", "desc"])
    )

    @order = case order_params.column
    when "service_kind"
      @order.new(service_kind_directions, true, service_type_directions, false)
    when "service_type"
      @order.new(service_kind_directions, false, service_type_directions, true)
    else
      @order.new(service_kind_directions, false, service_type_directions, false)
    end
  end

  def dashboard_order_params
    case order_params.column
    when "service_kind"
      "service_kinds.title #{order_params.direction}"
    when "service_type"
      "action_kinds.title #{order_params.direction}"
    else
      "service_kinds.title ASC"
    end
  end

  def order_params
    sort_attributes = (params[:order] || "").split(" ").presence || ["service_kind", "asc"]
    Struct.new(:column, :direction).new(*sort_attributes)
  end
end
