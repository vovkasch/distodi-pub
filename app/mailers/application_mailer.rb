class ApplicationMailer < ActionMailer::Base
  default from: "support@distodi.com"
  layout 'mailer'
end
