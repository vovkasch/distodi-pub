# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

every 1.day, at: '3:00 am' do
  script "backup"
end

every 1.day, at: '3:15 am' do
  emails = %w( scherbina.v@gmail.com irinakrejcarova@gmail.com anna.sobol.92@gmail.com )
  rake "db:dump db:dump:send EMAILS=\"#{emails.join(", ")}\""
end

every 1.day, at: '4:30 am' do
  rake "remind:not_approved_services"
end

# Learn more: http://github.com/javan/whenever
